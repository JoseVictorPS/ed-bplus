#ifndef BPLUS_H
#define BPLUS_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct bplustree{
  int activeKeys, isLeaf, *keys;
  struct bplustree **children, *next;
}BPlus;

void readT();
BPlus *create();
BPlus *initialize();
void frees(BPlus *a);
BPlus *search(BPlus *a, int id);
void prints(BPlus * a);
BPlus *inserts(BPlus *T, int id);

#endif //BPLUS_H
