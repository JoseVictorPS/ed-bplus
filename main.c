#include "bplus.h"

//gcc bplus.c main.c -o bplus

int main(void){
  BPlus * arvore = initialize();
  int num = 0, from, to;
  while(num != -1){
    printf("Digite um numero para adicionar. 0 para imprimir e -1 para sair\n");
    scanf("%i", &num);
    if(num == -1){
      printf("\n");
      prints(arvore);
      frees(arvore);
      return 0;
    }
    /*
    else if(num == -9){
      scanf("%d", &from);
      arvore = retira(arvore, from, t);
      printTree(arvore);
    }
    */
    else if(!num){
      printf("\n");
      prints(arvore);
    }
    else arvore = inserts(arvore, num);
    printf("\n\n");
  }
}
