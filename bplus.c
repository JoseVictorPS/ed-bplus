#include "bplus.h"

/* (OBS 1) "i" as index for both keys and children:
* if "i" is index for a key "k", then it will also be
* the index for the last child that has the smaller than "k" elements*/

/* (OBS 2) Iteration through the activeKeys:
* when i < activeKeys, the index "i" iterates through the entire array of keys
* when i <= activeKeys, the index "i" iterates through the entire array of children
* activKeys: index of last child; activKeys-1: index of last key*/

/* (OBS 3) the leaf level contains all elements in the tree, each leaf carry one extra key
 * for the elements in the index level of the tree(except the first leaf)*/

/* (OBS 4) what happens to the key that goes up DURING spliting:
* when already up, it's right child(called "sibling") will hold it's key */

/* (OBS 5) A full node (2*t)-1 always holds an odd number of keys, so
* it'll have a key in the middle, which we call by "half" */

static BPlus *insertNotFull(BPlus *x, int id);
static BPlus *splits(BPlus *p, int n, BPlus* c);
static void printTree(BPlus *a, int depth);

int t = 2;

void readT() {
  printf("Digite o valor t: ");
  scanf("%d", &t);
}

BPlus *create(){
  //Allocates memory for a new node
  BPlus* newT = (BPlus*)malloc(sizeof(BPlus));

  //Sets bytes from newT to 0
  if(newT) memset(newT, 0, sizeof(BPlus));

  //Sets number of active keys to 0
  newT->activeKeys = 0;

  //Allocates array of integer keys
  newT->keys =(int*)malloc(sizeof(int)*((t*2)-1));

  //Sets bytes from array keys to 0
  if(newT->keys) memset(newT->keys, 0, sizeof(int)*((t*2)-1));

  //Sets bolean for leaf to true
  newT->isLeaf = 1;

  //Allocates array of pointers to children
  newT->children = (BPlus**)malloc(sizeof(BPlus*)*t*2);

  //Sets next node to null
  newT->next = NULL;

  int i;
  //Fills the children pointers to null
  for(i=0; i<(t*2); i++) newT->children[i] = NULL;

  //Returns the new node
  return newT;
}



BPlus *initialize(){
  //Returns null to the calling pointer
  return NULL;
}



void frees(BPlus *a){
  //If the node ain't null
  if(a){

    //It it ain't a leaf either
    if(!a->isLeaf){

      int i;
      //Calls recursion to it's children(OBS 2)
      for(i = 0; i <= a->activeKeys; i++) frees(a->children[i]);
    }

    //Frees the array of keys
    free(a->keys);

    //Frees the array of children
    free(a->children);

    //Frees the node
    free(a);
  }
}



BPlus *search(BPlus *a, int id){
  #ifndef notFound
  #define notFound NULL

  //If there's no node
  if (!a) return notFound;

  int i = 0;
  //Find the index where id is supposed to be (OBS 2)
  while ((i < a->activeKeys) && (id > a->keys[i])) i++;

  //If id occupies the index and the node is leaf, returns the node
  if ((i < a->activeKeys) && (a->isLeaf) && (id == a->keys[i])) return a;

  //If the key where id should be is bigger than id and there's no child
  if (a-> isLeaf) return notFound;

  //"i" points to where id could be found (OBS 1)
  if (a->keys[i] == id) i++;

  //Searches for the child where id could be found
  return search(a->children[i], id);

  #endif
}



void prints(BPlus * a) {
  //Calls auxiliar funcion with the encapsulated params
  printTree(a, 0);
}



static void printTree(BPlus *a, int depth){
  //If the node ain't null
  if(a){

    int i,j;
    //Iterates through the index of keys (OBS 2)
    for(i=0; i<=a->activeKeys-1; i++){

      //Calls recursion to it's children
      printTree(a->children[i],depth+1);

      //Prints blankspace proportional to it's depth
      for(j=0; j<=depth; j++) printf("   ");

      //Prints the key and newline
      printf("%d\n",a->keys[i]);
    }

    //Calls recursion to last child
    printTree(a->children[i],depth+1);
  }
}



static BPlus *splits(BPlus *p, int n, BPlus* c){
  /*The child "c" will hold the smaller keys, and the sibling "s"
  * will hold the bigger ones*/

  //Creates the node to be paired with the child "c", the Sibling "s"
  BPlus *s = create();

  //The sibling is in the same depth as the child, so...
  s->isLeaf = c->isLeaf;

  int j;
  //If the child "c" is not a leaf(not in the TLSE)
  if(!c->isLeaf){

    /* When the node with (2*t)-1 keys splits, 1 key goes up, so...
    * ((2*t)-1)-1 = (2*t)-2 = 2*(t-1)
    * When spliting, the node is divided in half, so...
    * (2*(t-1))/2 = t-1 */ 
    s->activeKeys = t-1;

    //From the first element after the half to the last key(OBS 5)
    for(j=0;j<t-1;j++) s->keys[j] = c->keys[j+t];

    //From the first child after the half to the last (OBS 5)
    for(j=0;j<t;j++){
      s->children[j] = c->children[j+t];
      c->children[j+t] = NULL;
    }
  }

  //If the child is a leaf(in the TLSE)
  else {

    //The sibling will have one extra key if it's in the leaf level(OBS 3)
    s->activeKeys = t;

    //Assigns the keys from the half of the array to the last key(OBS 5 & 4 & 3)
    for(j=0;j < t;j++) s->keys[j] = c->keys[j+t-1];

    //The child will point to the sibling in the TLSE
    c->next = s;
  }

  //The child will hold the keys from the first(inclusive) to the half(exclusive)
  c->activeKeys = t-1;

  //Pushes the children foward
  //From the first child after "c" to the last
  for(j=p->activeKeys; j>=n; j--) p->children[j+1]=p->children[j];

  //Assigns the first child after "c" to be the sibling "s"
  p->children[n] = s;

  //Pushes the keys foward
  //From the key with the same index as "c" to the last active one(OBS 1)
  for(j=p->activeKeys; j>=n; j--) p->keys[j] = p->keys[j-1];

  //The key in the middle of the child "c", goes up to the index
  /* where holds the key bigger than the elements in the child "c" (OBS 1) */
  p->keys[n-1] = c->keys[t-1];

  //The key that went up
  p->activeKeys++;

  //Returns the updated parent node
  return p;
}



static BPlus *insertNotFull(BPlus *x, int id){
  //Assigns i to last index of keys
  int i = x->activeKeys-1;

  //Insertion in the node
  if(x->isLeaf){

    //Searchs the index of the last element smaller than k
    while((i>=0) && (id < x->keys[i])){

      //Pushes foward the elements
      x->keys[i+1] = x->keys[i];
      i--;
    }

    //Assigns k to the right position
    x->keys[i+1] = id;

    //The new key that we just inserted
    x->activeKeys++;

    //Returns the updated node
    return x;
  }

  //If node isn't a leaf (Insertion on it's child)

  //Searchs the index of the last key smaller than k
  while((i>=0) && (id < x->keys[i])) i--;

  /*Assigns i to the first child with bigger keys than
  * the last key with smaller keys than k*/
  i++;

  //If the child where k will be inserted is full
  if(x->children[i]->activeKeys == ((2*t)-1)){

    //splits the child node updating the current node
    x = splits(x, (i+1), x->children[i]);

    //If the key that came up is smaller than k
    if(id > x->keys[i]) i++;
  }

  //Inserts in the child "i", or it's grandchild
  x->children[i] = insertNotFull(x->children[i], id);

  //Returns the updated node
  return x;
}

BPlus *inserts(BPlus *T, int id){
  //If the id is already in the tree
  if(search(T, id)) return T;

  //If the tree is empty
  if(!T){

    //Allocates memory for the node
    T=create(t);

    //Assigns the first key to be id
    T->keys[0] = id;

    //The one key we just inserted
    T->activeKeys=1;

    //Returns the updated node
    return T;
  }

  //If the array of keys is full
  if(T->activeKeys == (2*t)-1){

    //Creates a empty node to be the new root
    BPlus *newRoot = create();

    //Sets boolean for leaf to false
    newRoot->isLeaf = 0;

    //Sets the first child to the previus root
    newRoot->children[0] = T;

    //Splits the old root bringing up one key to the new root
    newRoot = splits(newRoot,1,T);

    //Inserts the element in one of the children of the new root
    newRoot = insertNotFull(newRoot, id);

    //Returns the new root
    return newRoot;
  }

  //Insertion for a not full node
  T = insertNotFull(T, id);

  //Returns updated node
  return T;
}